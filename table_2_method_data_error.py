import csv
import pandas as pd
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing

import json

def linear_fit(x, a, b):
       return a*x + b
    
def exponential_fit(x, a, b, c):
       return a*np.exp(b*x) + c

def table_2_method_error(filejs):
    data = json.loads(filejs)
    X = list(map(float, list(data.keys())))
    y_fit = list(map(float, list(data.values())))
    date_2019 = 1546300800
    date_2024 = 1704067200
    before = np.linspace(date_2019, X[0], len(X))
    after = np.linspace(X[len(X) - 1], date_2024, len(X))
    answers_x = list(before) + list(X) + list(after)
    answers_x_fit = preprocessing.scale(answers_x)
    x_fit = answers_x_fit[len(X): 2*len(X)]
    # linear_function
    fitting_parameters, covariance = curve_fit(linear_fit, x_fit, y_fit, maxfev= 2000)
    a, b = fitting_parameters
    N = 12
    time_start = answers_x[0]
    time_end = answers_x[len(answers_x) - 1] 
    answers_x = np.linspace(time_start, time_end, N)
    time_start_fit = answers_x_fit[0]
    time_end_fit =answers_x_fit[len(answers_x_fit) - 1]
    answers_x_fit = np.linspace(time_start_fit, time_end_fit, N)
    answers_y = linear_fit(answers_x_fit, a, b)
    
    #additional data
    aleph = 0.02 #509453 
    beta = 1.4 #8425442 
    gamma = -0.02 #.05040531
    M = N
    answers_x_fit_add = np.linspace(time_start_fit, time_end_fit, M)
    answers_y_add = exponential_fit(answers_x_fit_add, aleph, beta, gamma)
    answers_x_add = np.linspace(time_start, time_end, M)
    answers_x_final = np.zeros(N)
    answers_y_final = np.zeros(N)
    
    if np.all(answers_x_add == answers_x):
        for i in range(N):
            answers_x_final[i] = answers_x[i]
            answers_y_final[i] = (answers_y_add[i] + answers_y[i])/2.
    else:
        answers_x_final = list(answers_x) + list(answers_x_add)
        answers_y_final = list(answers_y) + list(answers_y_add)
            
    answer = json.dumps(dict(zip(answers_x_final, answers_y_final)))
    return (answer)

df = pd.read_csv('result_zdravo.csv', decimal = ',')
filejs = json.dumps(dict(zip(df['timestamp'].values.astype(float), df['value'].values.astype(float))))

error_data = table_2_method_error(filejs)
print(error_data)