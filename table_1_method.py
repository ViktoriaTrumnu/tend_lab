from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)

import pymongo
import pprint
from bson.objectid import ObjectId


uri = "mongodb://dbadmin:654321@185.22.60.14/?authSource=blogerCounter&authMechanism=SCRAM-SHA-1"
conn = pymongo.MongoClient(uri)
db = conn.blogerCounter
check = db.checks

import csv
import pandas as pd
from scipy.optimize import curve_fit
import numpy as np
from sklearn import preprocessing
import json

def exponential_fit(x, a, b, c):
       return a*np.exp(b*x) + c


@app.route('/')
def hello_world():
    date_2019 = 1546300800
    date_2024 = 1704067200

    df = pd.read_csv('Table_1.csv', decimal = ',')
    X = df['Date'].values.astype(float)
    y_fit = df['Quality'].values.astype(float)

    before = np.linspace(date_2019, X[0], len(X))

    after = np.linspace(X[len(X) - 1], date_2024, len(X))
    
    answers_x = list(before) + list(X) + list(after)

    answers_x_fit = preprocessing.scale(answers_x)

    x_fit = answers_x_fit[len(X): 2*len(X)]

    fitting_parameters, covariance = curve_fit(exponential_fit, x_fit, y_fit, maxfev=800)
    a, b, c = fitting_parameters

    N = 100
    time_start = answers_x[0]
    time_end = answers_x[len(answers_x) - 1] 
    answers_x = np.linspace(time_start, time_end, N)

    time_start_fit = answers_x_fit[0]
    time_end_fit =answers_x_fit[len(answers_x_fit) - 1]
    answers_x_fit = np.linspace(time_start_fit, time_end_fit, N)
    answers_y = exponential_fit(answers_x_fit, a, b, c)
    answers_y = answers_y - answers_y[0]     
    answer = json.dumps(dict(zip(answers_x, answers_y)))
    return(answer)

if __name__ == '__main__':
    app.run()