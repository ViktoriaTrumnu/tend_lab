#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[ ]:





# In[1]:


import csv
import pandas as pd


df = pd.read_csv('table_test.csv', decimal = ',')
dates = df['date']
print(dates[0])
df['quantity'] = df['quantity'].astype(float)
quantities = df['quantity']

print(quantities[0])


# In[2]:


len(quantities)


# In[15]:


import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

def exponential_fit(x, a, b, c):
       return a*np.exp(-b*x) + c


if __name__ == "__main__":
       #x = np.array([0, 0.5, 2, 1, 0.1, 3])
        x = np.zeros(len(quantities))
        for i in range(len(quantities)):
            x[i] = i
       #y = np.array([1, 1.4, 8.6, 2.7, 1.2, 20.1])
        y = quantities
        fitting_parameters, covariance = curve_fit(exponential_fit, x, y, maxfev = 2000)
        a, b, c = fitting_parameters
        print(fitting_parameters)

        next_x = 6
        next_y = exponential_fit(next_x, a, b, c)
        print(next_y)

        plt.plot(y)
        plt.plot(np.append(y, next_y), 'ro')
        plt.plot(t, exponential_fit(t, a, b, c) , label="Fitted Curve")
        plt.show()


# In[16]:


import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

def exponential_fit(x, a, b, c):
       return a*np.log(x) + b*x + c


if __name__ == "__main__":
        x = np.zeros(len(quantities))
        for i in range(len(quantities)):
            x[i] = i+1
        y = quantities
        fitting_parameters, covariance = curve_fit(exponential_fit, x, y, maxfev=800)
        a, b, c = fitting_parameters

        next_x = len(quantities) + 10
        next_y = exponential_fit(next_x, a, b, c)
        print(next_y)
        t = np.append(x, next_x)
        plt.plot(x, y)
        print(fitting_parameters)
        plt.plot(np.append(x, next_x), np.append(y, next_y), 'ro')
        plt.plot(t, exponential_fit(t, a, b, c) , label="Fitted Curve")

        plt.show()


# In[ ]:





# In[ ]:




