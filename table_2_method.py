from scipy.optimize import curve_fit
import numpy as np
from sklearn import preprocessing
import math
import json

def exponential_fit(x, a, b, c):
       return a*np.exp(b*x) + c
    
def inverse_exp(x,a,b,c):
    return np.log((x-c)/a)/b
    
    
def table_2_method(filejs):
    
    data = json.loads(filejs)
    X = list(map(float, list(data.keys())))
    y_fit = list(map(float, list(data.values())))
    date_2019 = 1546300800
    date_2024 = 1735689599

    before = np.linspace(date_2019, X[0], len(X))
    after = np.linspace(X[len(X) - 1], date_2024, len(X))
    answers_x = list(before) + list(X) + list(after)
    answers_x_fit = np.array(answers_x)
    mean = answers_x_fit.mean()
    std = answers_x_fit.std()**2
   
    answers_x_fit = (answers_x_fit - mean)/(std)**(0.5)
    
    x_fit = answers_x_fit[len(X): 2*len(X)]
    
    fitting_parameters, covariance = curve_fit(exponential_fit, x_fit, y_fit, maxfev= 2000)
    a, b, c = fitting_parameters

    N = 60
    time_start = answers_x[0]
    time_end = answers_x[len(answers_x) - 1] 
    answers_x = np.linspace(time_start, time_end, N)

    time_start_fit = answers_x_fit[0]
    time_end_fit =answers_x_fit[len(answers_x_fit) - 1]
    answers_x_fit = np.linspace(time_start_fit, time_end_fit, N)
    answers_y = exponential_fit(answers_x_fit, a, b, c)
    answers_y = answers_y - answers_y[0]
    norm_done_value = inverse_exp(1,a,b,c)
    done_value = math.ceil(norm_done_value * ((std)**(0.5)) +  mean)
    dif_days = math.ceil((date_2024 - done_value)/(60*60*24)) # в миллисек надо еще на 1000 делить (60*60*24*1000)
    #если dif_days < 0 => мы отстаем от графика на столько дней, иначе обгоняем
    corr = (1 - answers_y[len(answers_y) - 1]) * 100
    if corr <= 0:
        corr = 0
    # На corr% нужно увеличить темп, чтобы успеть в срок
    answer = json.dumps(dict(zip(answers_x, answers_y)))
    return (answer, dif_days, corr )
